const projectPath = process.env.CI_PROJECT_PATH; // eslint-disable-line no-undef
const jobId = process.env.CI_JOB_ID; // eslint-disable-line no-undef

const [topLevelGroup, ...remainingPath] = projectPath.split('/');

const reviewUrl = `https://${topLevelGroup}.gitlab.io/-/${remainingPath.join('/')}/-/jobs/${jobId}/artifacts/public/index.html`

console.info(reviewUrl);
